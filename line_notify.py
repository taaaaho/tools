#!/usr/bin/python3
# -*- coding: utf-8 -*-
import requests

def lineNotify(message):
    line_notify_token = '取得したトークンを指定'
    line_notify_api = 'https://notify-api.line.me/api/notify'
    payload = {'message': message}
    headers = {'Authorization': 'Bearer ' + line_notify_token} 
    requests.post(line_notify_api, data=payload, headers=headers)

lineNotify('test')

